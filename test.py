import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as spo

# Function to find date where Adj Close != Close in csv ----------------------------------------------------
def test_datefinder():
    df = pd.read_csv("data/IBM.csv")
    print df

# TWO PLOT --------------------------------------------------------------------------------------------------
def test_mpl():
    df = pd.read_csv("data/IBM.csv", index_col='Date')
    df[['Close', 'Adj Close']].plot()
    plt.show()

# JOIN ------------------------------------------------------------------------------------------------------
def test_join():
    start_date = '2010-01-22'
    end_date = '2010-01-26'
    dates = pd.date_range(start_date, end_date)
    df1 = pd.DataFrame(index=dates)
    df2_SPY = pd.read_csv("data/SPY.csv", index_col="Date",
                          parse_dates=True,
                          usecols=['Date', 'Adj Close'],
                          na_values=['NaN']  # NaN should be not a number
                          )
    dfR = df1.join(df2_SPY)  # NaNs are still in it want to drop it
    print dfR
    dfR = dfR.dropna()
    print dfR

# DAILY RETURNS ----------------------------------------------------------------------------------------------
def test_return():
    dates = pd.date_range('2010-01-01', '2010-12-31')  # one month only
    symbols = ['GOOG', 'IBM', 'GLD']
    df = get_data(symbols, dates)
    plot_data(df)
    daily_returns = compute_daily_returns(df)
    plot_data(daily_returns, title="Daily returns", ylabel="Daily returns")

def compute_daily_returns(df):
    daily_returns = (df/df.shift(1)) - 1
    daily_returns.ix[0, :] = 0
    return daily_returns

def plot_data(df, title="Stock prices", xlabel="Date", ylabel="Price"):
    ax = df.plot(title=title, fontsize=12)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.show()

def get_data(symbols, dates):
    df = pd.DataFrame(index=dates)
    if 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols.insert(0, 'SPY')

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':
            df = df.dropna(subset=["SPY"])
    return df

def symbol_to_path(symbol, base_dir="data"):
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))
# --------------------------------------------------------------------------------------------------------------

def f(X):
    Y = (X - 1.5)**2 + 0.5
    print "X = {}, Y = {}".format(X, Y)
    return Y

def test_run():
    Xguess = 2.0
    min_result = spo.minimize(f, Xguess, method='SLSQP', options={'disp': True})
    print "Minima found at:"
    print "X = {}, Y = {}".format(min_result.x, min_result.fun)
    Xplot = np.linspace(0.5, 2.5, 21)
    Yplot = f(Xplot)
    plt.plot(Xplot, Yplot)
    plt.plot(min_result.x, min_result.fun, 'ro')
    plt.title("Minima of an objective function")
    plt.show()

def f2(X):
    Y = X[0]**2 + X[1]**2
    print "X1 = {}, X2 = {}, Y = {}".format(X[0], X[1], Y)
    return Y

def test_run2():
    X1_guess = [1, 1]
    min_result = spo.minimize(f2, X1_guess, method='SLSQP', options={'disp': True})
    print "Minima found at:"
    print "X1 = {}, Y = {}".format(min_result.x, min_result.fun)
# --------------------------------------------------------------------------------------------------------------



if __name__ == "__main__":
    test_return()
