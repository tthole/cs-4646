"""MC1-P2: Optimize a portfolio.

Copyright 2017, Georgia Tech Research Corporation
Atlanta, Georgia 30332-0415
All Rights Reserved
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data

import argparse
from itertools import count
import scipy.optimize as spo
from math import isnan
from analysissol import get_portfolio_value, get_portfolio_stats

# MH: Making data as input parameters work
# https://stackoverflow.com/questions/8080794/
#       in-python-how-do-i-split-a-string-to-a-number-of-integers
def str2dt(strng):
    year,month,day = map(int,strng.split('-'))
    return dt.datetime(year,month,day)

def find_optimal_allocations(prices):
    """Find optimal allocations for a stock portfolio, optimizing for both:
		volatility ratio.
      		Sharpe ratio.
	returns both.

    Parameters
    ----------
        prices: daily prices for each stock in portfolio

    Returns
    -------
        allocsVOL: optimal allocations, as fractions that sum to 1.0
        allocsSR: optimal allocations, as fractions that sum to 1.0
    """

    # ----------------------------------------------------------------------------------------
    # function called by optimizer
    def eval_allocations_VOL(allocs):
        allocs = allocs / np.sum(allocs)  # normalize allocations

        # from  analysis.py construct portolio
        port_val = get_portfolio_value(prices, allocs)

        # from  analysis.py compute statistics to  std_daily_ret for volatility
	# ----> std_daily_ret
        cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = get_portfolio_stats(port_val)

        # returning volatility is simply the return from get_portfolio_stats
        retval = std_daily_ret

        # handle the case return value is NaN
        return 1.0 if isnan(retval) else retval

    # ----------------------------------------------------------------------------------------
    # function called by optimizer
    def eval_allocations_SR(allocs):
        """"Evaluate given allocations, return negated Sharpe ratio for minimization."""
	retval = 1.0

        # handle the case return value is NaN
        return 1.0 if isnan(retval) else retval

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
    # BEGIN ----- find_optimal_allocations code optimizer calls functions above

    initial_allocs = [1.0 / prices.shape[1]] * prices.shape[1]  # equal alloc at first
    bounds = ((0.0, 1.0),) * prices.shape[1]  # bounds for each stock b/w 0.0, and 1.0

    # QA; if you constraint all values ot be less than 50 how would you
    # re-define the constrints?
    constraints = ({ 'type':   'eq', 'fun': lambda allocs: 1.0 - np.sum(allocs) })

    result_volatility = \
        spo.minimize(
                eval_allocations_VOL,
                initial_allocs,
                method='SLSQP',
                bounds=bounds,
                constraints=constraints)

    result_VOL = result_volatility.x

    result_SR = result_volatility.x  # QA change to optimize on SR

    return result_VOL, result_SR


# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def optimize_portfolio(\
	sd=dt.datetime(2008,1,1),\
	ed=dt.datetime(2009,1,1),\
    	syms=['GOOG','AAPL','GLD','XOM'],\
	gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # find the allocations for the optimal portfolio
    # note that the values here ARE NOT meant to be correct for a test case
    allocs = np.asarray([0.2, 0.2, 0.3, 0.3]) # add code here to find the allocations

    # return allocation for 2 different  metrics 
    allocsVOL, allocsSR = find_optimal_allocations(prices)

    sum_allocsVOL = np.sum( allocsVOL )
    sum_allocsSR = np.sum( allocsSR )
    allocsVOL = allocsVOL / sum_allocsVOL  # normalize allocations, 
    allocsSR = allocsSR / sum_allocsSR  # normalize allocations 

    port_valVOL = get_portfolio_value(prices, allocsVOL)
    port_valSR  = get_portfolio_value(prices, allocsVOL)  # QA: change to SR

    cum_retV, avg_daily_retV, std_daily_retV, sharpe_ratioV = get_portfolio_stats(port_valVOL)
    cum_retSR, avg_daily_retSR, std_daily_retSR, sharpe_ratioSR = get_portfolio_stats(port_valSR)

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        # df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)

        normed_SPY = prices_SPY / prices_SPY.ix[0, :]
        normed_PORTVAL_V =  port_valVOL / port_valVOL.ix[0, :]
        normed_PORTVAL_SR = port_valSR  / port_valSR.ix[0, :]

        df_temp = pd.concat(
                [normed_PORTVAL_V, normed_PORTVAL_SR, normed_SPY],
                keys=[  'Portfolio (Volatility)',
                        'Portfolio (Sharp Ratio)',
                        'SPY'], axis=1)
        plot_data(df_temp, title="Daily Portfolio Value and SPY\n"\
                                "{}".format(syms)
                )

        pass

    return allocsVOL, cum_retV, avg_daily_retV, avg_daily_retV, sharpe_ratioV

def test_code(gp=True,\
        sd_str = '2010-01-01',\
        ed_str = '2010-12-31',\
        symbols=['GOOG', 'AAPL', 'GLD', 'XOM', 'IBM']):

    # This function WILL NOT be called by the auto grader
    # Do not assume that any variables defined here are available to your function/code
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!

    symbols = list(symbols)
    start_date  = str2dt(sd_str)
    end_date    = str2dt(ed_str)
    if len(symbols)==0:
    	symbols = ['GOOG', 'AAPL', 'GLD', 'XOM', 'IBM']

    # Assess the portfolio
    allocations, cr, adr, sddr, sr = optimize_portfolio(\
	sd = start_date,\
	ed = end_date,\
        syms = symbols,\
        gen_plot = gp)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    # This code WILL NOT be called by the auto grader
    # Do not assume that it will be called

    plot            =  False
    stocks          =  ['GOOG', 'AAPL', 'GLD', 'XOM']
    start_date      = '2010-01-01'
    end_date        = '2010-12-31'

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', action="store_true", dest="plot", default=False)
    parser.add_argument('-s', action="store", dest="start_date",default=start_date)
    parser.add_argument('-e', action="store", dest="end_date",default=end_date)
    parser.add_argument('-x', nargs='*',action="store", dest="stocks",default=stocks)

    # w/ line below 
    # python optimization-ACT.py -p -s 2010-01-01 -e 2010-12-31  -x 'GOOG' 'AAPL' 'GLD' 'XOM'
    print "python optimization-ACT.py ",
    print "-p -s 2010-01-01 -e 2010-12-31  -x 'GOOG' 'AAPL' 'GLD' 'XOM'"
    args = parser.parse_args()

    if args.plot:
        import matplotlib.pyplot as plt

    print "-p {:<10} (plotting)".format( args.plot )
    print "-x {:<10} (stocks)".format( args.stocks )
    print "-s {:<10} (start date)".format( args.start_date )
    print "-e {:<10} (end date)".format( args.end_date )
    print ""

    test_code( args.plot, args.start_date, args.end_date, list(args.stocks) )

