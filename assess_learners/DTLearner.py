import numpy as np

class DTLearner(object):
    def __init__(self, verbose=False, leaf_size = 1):
        self.leaf_size = leaf_size
        self.tree = None

    def author(self):
        return 'tthole3'

    def addEvidence(self, x_data, y_data):
        self.tree = self.make_tree(np.concatenate((x_data, y_data[:, None]), axis=1))

    def make_tree(self, data):
        index = 0
        max = 0
        first_data = data[:, -1]
        second_data = data[:, 0:-1]
        unique = np.unique(first_data)
        if unique.shape[0] == 1:
            return np.array([[-1, unique[0], np.NAN, np.NAN]], dtype=object)
        elif data.shape[0] <= self.leaf_size:
            return np.array([[-1, np.mean(data[:, -1]), np.NAN, np.NAN]], dtype=object)
        for i in range(second_data.shape[1]):
            if np.correlate(second_data[:, i], first_data) > max:
                index = i
                max = np.correlate(second_data[:, i], first_data)
        index_data = data[:, index]
        splitter = np.median(index_data)
        if np.array_equal(data[index_data <= splitter], data):
            return np.array([[-1, np.mean(data[:, -1]), np.NAN, np.NAN]], dtype=object)
        else:
            left_tree = self.make_tree(data[index_data <= splitter])
            right_tree = self.make_tree(data[index_data > splitter])
            root = np.array([[index, splitter, 1, left_tree.shape[0] + 1]], dtype=object)
            return np.concatenate((root, left_tree, right_tree), axis=0)

    def splitter(self, point, i):
        branch0, branch1, branch2, branch3 = self.tree[i, :]
        if branch0 == -1:
            self.build.append(branch1)
            pass
        elif point[branch0] <= branch1:
            self.splitter(point, i + branch2)
        else:
            self.splitter(point, i + branch3)

    def query(self, points):
        self.build = []
        for i in range(points.shape[0]):
            self.splitter(points[i], 0)
        return self.build


if __name__=="__main__":
    print "the secret clue is 'zzyzx'"
