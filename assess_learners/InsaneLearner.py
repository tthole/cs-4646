import BagLearner as bl, LinRegLearner as lrl, numpy as np
class InsaneLearner(object):
    def __init__(self, verbose=False):
        learners = []
        self.verbose = verbose
        for i in range(20): learners.append(bl.BagLearner(20, {}, lrl.LinRegLearner))
        self.learners = learners
    def addEvidence(self, x_trainer, y_trainer):
        for learner in self.learners: learner.addEvidence(x_trainer, y_trainer)
    def query(self, x_tester):
        build = np.empty((x_tester.shape[0], 20))
        for column in range(20): build[:, column] = self.learners[column].query(x_tester)
        return build.mean(axis=1)
    def author(self): return 'tthole3'