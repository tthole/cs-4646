"""
template for generating data to fool learners (c) 2016 Tucker Balch
"""

import numpy as np
import math

# this function should return a dataset (X and Y) that will work
# better for linear regression than decision trees
def best4LinReg(seed=np.random.randint(1000000)):
    np.random.seed(seed)
    rows = np.random.randint(10, 1001)
    columns = np.random.randint(2, 1001)
    X = np.random.normal(size=(rows, columns))
    Y = np.zeros(rows)
    for column in range(columns): Y += X[:, column]
    # Here's is an example of creating a Y from randomly generated
    # X with multiple columns
    # Y = X[:,0] + np.sin(X[:,1]) + X[:,2]**2 + X[:,3]**3
    return X, Y

def best4DT(seed=np.random.randint(1000000)):
    np.random.seed(seed)
    rows = np.random.randint(10, 1001)
    columns = np.random.randint(2, 1001)
    X = np.random.normal(size=(rows, columns))
    Y = np.zeros(rows)
    for column in range(columns): Y += X[:, column] ** 2
    return X, Y

def author():
    return 'tthole3' #Change this to your user ID

if __name__=="__main__":
    print "they call me Tim."
