import numpy as np

class RTLearner(object):
    def __init__(self, verbose=False, leaf_size = 5):
        self.tree = None
        self.leaf_size = leaf_size
        pass

    def author(self):
        return 'tthole3'

    def addEvidence(self, x_data, y_data):
        self.tree = self.make_tree(np.concatenate((x_data, y_data[:, None]), axis=1))

    def make_tree(self, data):
        row = data.shape[0]
        if row <= self.leaf_size:
            return np.array([[-1, np.mean(data[:, -1]), np.NAN, np.NAN]], dtype=object)
        if np.unique(data[:, -1]).shape[0] == 1:
            return np.array([[-1, np.unique(data[:, -1])[0], np.NAN, np.NAN]], dtype=object)
        num = np.random.randint((data[:, 0:-1]).shape[1])
        num_data = data[:, num]
        splitter = (data[np.random.randint(row), num] + data[np.random.randint(row), num]) / 2
        if np.array_equal(data[num_data <= splitter], data):
            return np.array([[-1, np.mean(data[:, -1]), np.NAN, np.NAN]], dtype=object)
        left_tree = self.make_tree(data[num_data <= splitter])
        right_tree = self.make_tree(data[num_data > splitter])
        root = np.array([[num, splitter, 1, left_tree.shape[0] + 1]], dtype=object)
        return np.concatenate((root, left_tree, right_tree), axis=0)

    def splitter(self, point, i):
        branch0, branch1, branch2, branch3 = self.tree[i, :]
        if branch0 == -1:
            self.build.append(branch1)
            pass
        elif point[branch0] <= branch1:
            self.splitter(point, i + branch2)
        else:
            self.splitter(point, i + branch3)

    def query(self, nodes):
        self.build = []
        for i in range(nodes.shape[0]):
            self.splitter(nodes[i], 0)
        return self.build


if __name__ == "__main__":
    print "the secret clue is 'zzyzx'"
