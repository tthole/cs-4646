"""
Template for implementing StrategyLearner  (c) 2016 Tucker Balch

@author Thomas Thole tthole3
"""

import datetime as dt
import pandas as pd
import util as ut
import random
import numpy as np
from indicators import *
import RTLearner as rtl
import BagLearner as bag

class StrategyLearner(object):

    # constructor
    def __init__(self, verbose = False, impact=0.0):
        self.verbose = verbose
        self.impact = impact
        self.learner = bag.BagLearner(learner=rtl.RTLearner, kwargs={"leaf_size": 5}, bags=20, boost=False, verbose=False)

    # this method creates a Random Forest learner and trains it
    def addEvidence(self, symbol = "IBM", \
        sd=dt.datetime(2008,1,1), \
        ed=dt.datetime(2009,1,1), \
        sv = 10000):

        symbols = [symbol]
        dates = pd.date_range(sd, ed)
        back_check = 2
        prices_all = ut.get_data(symbols, dates)
        prices = prices_all[symbols]

        volatility = getVolatility(prices, back_check, symbols)
        sma = getSMA(prices, back_check, symbols)
        bollinger = getBollinger(prices, symbols, back_check)

        volatility_df = volatility.rename(columns={symbol: 'Volatility'})
        sma_df = sma.rename(columns={symbol:'SMA'})
        bollinger_df = bollinger.rename(columns={symbol:'Bollinger'})
        indicators = pd.concat((sma_df, bollinger_df, volatility_df), axis=1)
        indicators.fillna(0, inplace=True)
        indicators = indicators[:-5]
        trainX = indicators.values

        trainY = []
        for i in range(prices.shape[0] - 5):
            ratio = (prices.ix[i + 5, symbol] - prices.ix[i, symbol]) / prices.ix[i, symbol]
            if ratio < (-0.02 - self.impact):
                trainY.append(-1)
            elif ratio > (self.impact + 0.02):
                trainY.append(1)
            else:
                trainY.append(0)
        trainY = np.array(trainY)

        self.learner.addEvidence(trainX, trainY)

    # this method should use the existing policy and test it against new data
    def testPolicy(self, symbol = "IBM", \
        sd=dt.datetime(2009,1,1), \
        ed=dt.datetime(2010,1,1), \
        sv = 10000):

        symbols = [symbol]
        dates = pd.date_range(sd, ed)
        back_check = 2
        prices_all = ut.get_data(symbols, dates)
        prices = prices_all[symbols]

        volatility = getVolatility(prices, back_check, symbols)
        sma = getSMA(prices, back_check, symbols)
        bollinger = getBollinger(prices, symbols, back_check)

        volatility_df = volatility.rename(columns={symbol: 'Volatility'})
        sma_df = sma.rename(columns={symbol: 'SMA'})
        bollinger_df = bollinger.rename(columns={symbol: 'Bollinger'})
        indicators = pd.concat((sma_df, bollinger_df, volatility_df), axis=1)
        indicators.fillna(0, inplace=True)
        trainX = indicators.values

        testY = self.learner.query(trainX)
        trades = prices_all[symbols].copy()
        trades.loc[:] = 0
        check = 0
        for i in range(0, prices.shape[0] - 1):
            if check == 0:
                if testY[i] > 0:
                    trades.values[i, :] = 1000
                    check = 1
                elif testY[i] < 0:
                    trades.values[i, :] = -1000
                    check = -1
            elif check == 1:
                if testY[i] < 0:
                    trades.values[i, :] = -2000
                    check = -1
                elif testY[i] == 0:
                    trades.values[i, :] = -1000
                    check = 0
            else:
                if testY[i] > 0:
                    trades.values[i, :] = 2000
                    check = 1
                elif testY[i] == 0:
                    trades.values[i, :] = 1000
                    check = 0
        if check == -1:
            trades.values[prices.shape[0] - 1, :] = 1000
        elif check == 1:
            trades.values[prices.shape[0] - 1, :] = -1000

        return trades

    def author(self):
        return 'tthole3'

if __name__=="__main__":
    print "One does not simply think up a strategy"