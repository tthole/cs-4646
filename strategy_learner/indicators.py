"""
Indicator generation functions

@author Thomas Thole tthole3
"""

def getVolatility(prices, back_check, symbols):
    price = prices[symbols]
    volatility = price.rolling(window=back_check, center=False).std()
    return volatility

def getSMA(prices, back_check, symbols):
    price = prices[symbols]
    sma = price.rolling(window=back_check, center=False).mean()
    return sma

def getBollinger(prices, symbols, back_check):
    price = prices[symbols]
    average = price.rolling(window=back_check, center=False).mean()
    standard_dev = price.rolling(window=back_check, center=False).std()
    bollinger = (price - average) / (2 * standard_dev)
    return bollinger

def author(self):
    return 'tthole3'