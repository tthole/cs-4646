import numpy as np

class BagLearner(object):
    def __init__(self, bags, kwargs, learner, verbose = False, boost = False):
        self.bags = bags
        self.learners = []
        for i in range(self.bags):
            self.learners.append(learner(**kwargs))

    def author(self):
        return 'tthole3'

    def addEvidence(self, x_data, y_data):
        for i in range(self.bags): self.learners[i].addEvidence(x_data, y_data)

    def query(self,points):
        build = []
        for i in range(self.bags): build.append(self.learners[i].query(points))
        return np.mean(build, 0)


if __name__=="__main__":
    print "the secret clue is 'zzyzx'"
